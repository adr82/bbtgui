export PATH := c:/moregrasp/pythondeps/python/Library/bin:$(PATH)

all: bbtgui bbtgui_addstream bbtgui_addparameter bbtgui_addunit bbtgui_addmodule

bbtgui:
	pyuic5 -o bbtgui_ui.py -i 4 bbtgui.ui

bbtgui_addstream:
	pyuic5 -o bbtgui_addstream_ui.py -i 4 bbtgui_addstream.ui

bbtgui_addparameter:
	pyuic5 -o bbtgui_addparameter_ui.py -i 4 bbtgui_addparameter.ui

bbtgui_addunit:
	pyuic5 -o bbtgui_addunit_ui.py -i 4 bbtgui_addunit.ui

bbtgui_addmodule:
	pyuic5 -o bbtgui_addmodule_ui.py -i 4 bbtgui_addmodule.ui
