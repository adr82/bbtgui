import sys, os, logging
from six import iteritems

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QDialog

from bbtgui_ui import Ui_MainWindow
from bbtgui_addstream_ui import Ui_AddStreamDialog
from bbtgui_addparameter_ui import Ui_AddParameterDialog
from bbtgui_addunit_ui import Ui_AddUnitDialog
from bbtgui_addmodule_ui import Ui_AddModuleDialog

import bbtconfig as bcc

logger = logging.getLogger('bbtgui')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter('%(module)s.%(funcName)s:%(lineno)d  %(message)s'))
logger.addHandler(handler)

if 'BZI_FRAMEWORK_ROOT' in os.environ:
    BZI_FRAMEWORK_ROOT = os.environ['BZI_FRAMEWORK_ROOT']
    CONFIG_ROOT = os.path.join(os.path.abspath(BZI_FRAMEWORK_ROOT), 'config')
    if not os.path.exists(CONFIG_ROOT):
        logger.warn('Unable to list files in %s, using working directory' % CONFIG_ROOT)
        CONFIG_ROOT = os.getcwd()
else:
    CONFIG_ROOT = os.getcwd()

class TreeItem(object):
    """
    Data for items in the tree widget
    """

    def __init__(self, content, data, parent=None):
        self._parent = parent
        self.content = content
        self._data = data
        self.children = []

    def add_child(self, item):
        return self.children.append(item)

    def add(self, content, data=None):
        self.children.append(TreeItem(content, data, self))
        return self.children[-1]

    def named_child(self, n):
        for c in self.children:
            if c.content == n:
                return c
        raise IndexError('Not found: %s' % n)

    def child(self, at):
        if at >= len(self.children):
            return None
        return self.children[at]

    def num_children(self):
        return len(self.children)

    def parent(self):
        return self._parent

    def row(self):
        if self._parent is None:
            return 0

        return self._parent.children.index(self)

    def rows(self):
        return len(self.children)

    def columns(self):
        return 1

class TreeModel(QtCore.QAbstractItemModel):
    """
    Data model for the tree widget
    """

    ITEM_DATA_ROLE  = QtCore.Qt.UserRole + 1
    ITEM_ROLE      = QtCore.Qt.UserRole + 2

    def __init__(self, config, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.tree = self.constructModelFromConfig(config)

    def constructModelFromConfig(self, config):
        self.tree = TreeItem('root', config) # won't actually be visible in the tree

        # add the controller, manager, modules and streams as children of the root
        self.tree.add('Controller', config.controller)
        self.tree.add('Manager', config.manager)
        self.tree.add('Modules', config.modules)

        # add the streams defined in the controller
        controller = self.tree.named_child('Controller')
        controller_inputs = controller.add('Input streams', config.controller.inputs)
        controller_outputs = controller.add('Output streams', config.controller.outputs)
        for name, stream in iteritems(config.controller.inputs):
            controller_inputs.add(name, stream)
        for name, stream in iteritems(config.controller.outputs):
            controller_outputs.add(name, stream)

        # add the units and parameters defined in the manager
        manager = self.tree.named_child('Manager')
        manager_units = manager.add('Units', config.manager.units)
        for name, unit in iteritems(config.manager.units):
            manager_units.add(name, unit)
        manager_params = manager.add('Parameters', config.manager.parameters)
        for name, param in iteritems(config.manager.parameters):
            manager_params.add(name, param)

        # add the other modules
        modules = self.tree.named_child('Modules')
        for mod in config.sorted_sync_modules():
            mod_node = modules.add('%s' % (mod.name), mod)
            for u in mod.units:
                unit = mod_node.add(u, mod.units[u])
                unit_inputs = unit.add('Input streams', mod.units[u].inputs)
                unit_outputs = unit.add('Output streams', mod.units[u].outputs)
                unit_parameters = unit.add('Parameters', mod.units[u].parameters)
                for s in mod.units[u].inputs:
                    unit_inputs.add(s, mod.units[u].inputs[s])
                for s in mod.units[u].outputs:
                    unit_outputs.add(s, mod.units[u].outputs[s])
                for p in mod.units[u].parameters:
                    unit_parameters.add(p, mod.units[u].parameters[p])
        for mod in config.async_modules():
            mod_node = modules.add('%s' % (mod.name), mod)
            for u in mod.units:
                mod_node.add(u, mod.units[u])

        return self.tree

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return QtCore.QVariant()

        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            return item.content
        if role == TreeModel.ITEM_DATA_ROLE:
            return item._data
        if role == TreeModel.ITEM_ROLE:
            return item

        return QtCore.QVariant()

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags

        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation != QtCore.Qt.Horizontal or role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

        return "Configuration structure"

    def set_content(self, index, content):
        item = index.internalPointer()
        item.content = content
        self.dataChanged.emit(index, index)

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        item = index.internalPointer()
        parentItem = item.parent()
        if parentItem == self.tree:
            return QtCore.QModelIndex()

        return self.createIndex(parentItem.row(), 0, parentItem)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parentItem = self.tree
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.tree
        else:
            parentItem = parent.internalPointer()

        return parentItem.num_children()

    def columnCount(self, parent):
        if not parent.isValid():
            return self.tree.columns()
        else:
            return parent.internalPointer().columns()

class BbtGuiAddModuleDialog(QDialog, Ui_AddModuleDialog):

    def __init__(self, obj_parent, initial_port, initial_stage, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.module = None
        self.obj_parent = obj_parent
        self.cboModuleSyncAsync.addItems(['Synchronous', 'Asynchronous'])
        self.cboModuleGM.addItems(['C++', 'Matlab'])
        self.lineModuleName.setText('new module')
        self.cboModuleSyncAsync.currentIndexChanged.connect(self.type_changed)
        self.spinModuleBufferSize.setEnabled(False)

    def type_changed(self, int_index):
        if int_index == 0: # sync
            self.spinModuleStage.setEnabled(True)
            self.spinModuleBufferSize.setEnabled(False)
        else:
            self.spinModuleStage.setEnabled(False)
            self.spinModuleBufferSize.setEnabled(True)

    def accept(self):
        if self.verify():
            QDialog.accept(self)

    def get_data(self):
        name = self.lineModuleName.text()
        ip = self.lineModuleIp.text()
        port = self.spinModulePort.value()

        if self.cboModuleGM.currentIndex() == BbtGui.MOD_CPP:
            exe = bcc.BZI_MODULE_CPP
        else:
            exe = bcc.BZI_MODULE_MATLAB

        if self.spinModuleStage.isEnabled():
            stage = self.spinModuleStage.value()
            return (name, ip, port, exe, True, stage)
        if self.spinModuleBufferSize.isEnabled():
            bufferSize = self.spinModuleBufferSize.value()
            return (name, ip, port, exe, False, bufferSize)

    def verify(self):
        (name, ip, port, exe, is_sync, other) = self.get_data()

        if len(name) == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter a name for the module')
            return False

        if len(ip) == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter an IP address for the module')
            return False

        if is_sync:
            self.module = bcc.SyncModule(name, ip, port, exe, other, None, self.obj_parent)
        else:
            self.module = bcc.AsyncModule(name, ip, port, exe, other, None, self.obj_parent)

        return True

class BbtGuiAddUnitDialog(QDialog, Ui_AddUnitDialog):

    def __init__(self, obj_parent, parent=None):
        QDialog.__init__(self, parent)
        self.setupUi(self)
        self.unit = None
        self.obj_parent = obj_parent
        self.cboUnitType.addItems([bcc.BZI_UNITS[x] for x in range(0, bcc.BZI_UNIT_MAX + 1, 1)])

    def accept(self):
        if self.verify():
            QDialog.accept(self)

    def get_data(self):
        name = self.lineUnitName.text()
        utype = self.cboUnitType.currentIndex()
        uclass = self.lineUnitClass.text()
        return (name, utype, uclass)

    def verify(self):
        (name, utype, uclass) = self.get_data()

        if len(name) == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter a name for the unit')
            return False

        if len(uclass) == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter a class for the unit')
            return False

        if utype == bcc.BZI_UNIT_GENERIC:
            self.unit = bcc.GenericUnit(name, uclass,  None, self.obj_parent)
        elif utype == bcc.BZI_MODULE_MATLAB:
            self.unit = bcc.MatlabUnit(name, uclass, None, self.obj_parent)
        else:
            self.unit = bcc.DisplayUnit(name, uclass, None, self.obj_parent)
        return True

class BbtGuiAddParameterDialog(QDialog, Ui_AddParameterDialog):
    """
    Dialog class to allow the user to define and add a new parameter to the configuration
    """

    def __init__(self, obj_parent, is_init_param, parent=None):
        QDialog.__init__(self, parent)
        self.is_init_param = is_init_param
        self.setupUi(self)
        self.obj_parent = obj_parent
        self.parameter = None
        # initparameter elements are used to initialise stream objects. they can
        # only contain values that are either integers or vectors of integers, so
        # limit the types available in that case
        if is_init_param:
            self.cboParamType.addItems([bcc.BZI_TYPES[x] for x in [bcc.BZI_INT, bcc.BZI_INT_VEC]])
        else:
            self.cboParamType.addItems([bcc.BZI_TYPES[x] for x in range(0, bcc.BZI_TYPE_MAX + 1, 1)])

    def accept(self):
        if self.verify():
            QDialog.accept(self)

    def get_data(self):
        name = self.lineParamName.text()
        ptype = self.cboParamType.currentIndex()
        value = self.textParamValue.toPlainText()
        return (name, ptype, value)

    def verify(self):
        (name, ptype, value) = self.get_data()

        if len(name) == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter a name for the parameter')
            return False

        # verify all types
        if ptype == bcc.BZI_INT or ptype == bcc.BZI_DOUBLE:
            value = value.strip()
            try:
                if ptype == bcc.BZI_INT:
                    int(value)
                else:
                    float(value)
            except ValueError:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Invalid value for numeric parameter (%s)' % (value))
                return False

        if self.is_init_param:
            self.parameter = bcc.InitParameter(name, value)
        else:
            self.parameter = bcc.Parameter(name, value, ptype)
        self.parameter.parent = self.obj_parent
        return True

class BbtGuiAddStreamDialog(QDialog, Ui_AddStreamDialog):
    """
    Dialog class to allow the user to define and add a new stream to the configuration
    """

    TYPE_EVENT_STREAM = 0
    TYPE_SIGNAL_STREAM = 1

    def __init__(self, obj_parent, parent=None):
        QDialog.__init__(self)
        self.setupUi(self)
        self.stream = None
        self.obj_parent = obj_parent
        self.cboStreamType.currentIndexChanged.connect(self.stream_type_changed)
        self.spinStreamSamplingRate.setEnabled(False)
        self.cboStreamType.addItems(['EventStream', 'SignalStream'])
        self.btnSetInitValues.clicked.connect(self.reset_init_values)

    def stream_type_changed(self, int_index):
        if int_index == BbtGuiAddStreamDialog.TYPE_EVENT_STREAM:
            self.spinStreamSamplingRate.setEnabled(False)
            self.textStreamInitValue.setEnabled(True)
            self.btnSetInitValues.setEnabled(True)
        elif int_index == BbtGuiAddStreamDialog.TYPE_SIGNAL_STREAM:
            self.spinStreamSamplingRate.setEnabled(True)
            self.textStreamInitValue.setEnabled(False)
            self.btnSetInitValues.setEnabled(False)

    # overrides QDialog.accept()
    def accept(self):
        if self.verify():
            QDialog.accept(self)

    def reset_init_values(self):
        self.textStreamInitValue.setPlainText('0 ' * self.spinStreamChannels.value())

    def get_data(self):
        name = self.lineStreamName.text()
        blocksize = self.spinStreamBlockSize.value()
        channels = self.spinStreamChannels.value()
        sampling_rate = self.spinStreamSamplingRate.value()
        init_value = self.textStreamInitValue.toPlainText()
        stream_type = self.cboStreamType.currentIndex()
        return (name, blocksize, channels, sampling_rate, init_value, stream_type)

    def verify(self):
        (name, blocksize, channels, sampling_rate, init_value, stream_type) = self.get_data()

        if len(name) == 0:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Please enter a name for the stream')
            return False

        # if it's an event stream, check the init values are OK
        if stream_type == BbtGuiAddStreamDialog.TYPE_EVENT_STREAM:
            init_value = init_value.strip()

            init_value = init_value.replace('[', '')
            init_value = init_value.replace(']', '')
            # without params, split will return "words" separated by "arbitrary
            # strings of whitespace", which helpfully is exactly what is needed here
            init_values = init_value.split()

            # try to parse all the tokens as numbers (ints or floats are OK)
            try:
                init_values = list(map(float, init_values))
            except ValueError as e:
                logger.error('Failed to parse init value (%s)' % (str(e)))
                QtWidgets.QMessageBox.warning(self, 'Error', 'Bad number format in init values (must be integer or floating point')
                return False

            # check the number of init values matches the number of channels
            if len(init_values) != channels:
                QtWidgets.QMessageBox.warning(self, 'Error', 'Number of init values doesn\'t match number of channels (%d vs %d)' % (len(init_values), channels))
                return False

        if stream_type == BbtGuiAddStreamDialog.TYPE_EVENT_STREAM:
            self.stream = bcc.EventStream(name, channels, blocksize, self.obj_parent, '[%s]' % init_value.strip())
        else:
            self.stream = bcc.SignalStream(name, channels, blocksize, self.obj_parent, sampling_rate)
        return True

class BbtGui(QMainWindow, Ui_MainWindow):

    TAB_CONTROLLER = 0
    TAB_MANAGER    = 1
    TAB_MODULE     = 2
    TAB_UNIT       = 3
    TAB_STREAM     = 4
    TAB_PARAMETER  = 5
    TAB_NOTHING    = 6

    STREAM_EVENT   = 0
    STREAM_SIGNAL  = 1

    MOD_SYNC       = 0
    MOD_ASYNC      = 1

    MOD_CPP        = 0
    MOD_MATLAB     = 1

    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)

        # disable maximize button
        flags = self.windowFlags()
        flags ^= QtCore.Qt.WindowMaximizeButtonHint
        self.setWindowFlags(flags | QtCore.Qt.CustomizeWindowHint)

        self.cur_object = None
        self.cur_module = None
        self.cur_unit = None
        self.cur_stream = None
        self.cur_parameter = None
        self.config = None
        self.config_changed = False
        self.cur_tab = BbtGui.TAB_CONTROLLER

        # populate list type widgets
        self.cboModuleSyncAsync.addItems(['Synchronous', 'Asynchronous'])
        self.cboModuleGM.addItems(['C++', 'Matlab'])
        self.cboStreamType.addItems(['EventStream', 'SignalStream'])
        self.cboStreamType.setEnabled(False)
        self.cboParamType.addItems([bcc.BZI_TYPES[x] for x in range(0, bcc.BZI_TYPE_MAX + 1, 1)])
        self.cboUnitType.addItems([bcc.BZI_UNITS[x] for x in range(0, bcc.BZI_UNIT_MAX + 1, 1)])

        # set up all the event handlers etc

        # tab changes
        # self.tabEdit.currentChanged.connect(self.tab_changed)

        # main tab buttons
        self.btnNew.clicked.connect(self.new_config)
        self.btnLoad.clicked.connect(self.load_config)
        self.btnSave.clicked.connect(self.save_config)
        self.btnSaveAs.clicked.connect(self.save_config_as)

        # main tab config list
        self.cboConfigName.currentIndexChanged.connect(lambda x: self.load_model(os.path.join(CONFIG_ROOT, self.cboConfigName.itemText(x))))

        # main tab tree widget
        self.treeConfig.customContextMenuRequested.connect(self.show_context_menu)
        # self.treeConfig.clicked.connect(self.tree_clicked)

        # error/warning list
        self.listMessages.itemClicked.connect(lambda item: self.show_error_object(item))

        self.reload_configs() # this will also trigger a load of the first config in the list

        self.actionExit.triggered.connect(self.exit)
        self.actionCheckEnv.triggered.connect(self.check_env)

        # event handlers for all the other widgets. it's a bit messy but is basically
        # just wiring up data changed signals from widgets to setters on the current
        # object, updating the tree data model where required, and noting that the
        # configuration has new changes

        # controller
        self.lineConName.textEdited.connect(lambda x: (self.cur_object.set_name(x), self.set_config_changed()))
        self.spinConNumRetries.valueChanged.connect(lambda x: (self.cur_object.set_numRetries(x), self.set_config_changed()))

        # manager
        self.lineManName.textEdited.connect(lambda x: (self.cur_object.set_name(x), self.set_config_changed()))
        self.lineManIp.textEdited.connect(lambda x: (self.cur_object.set_ip(x), self.set_config_changed()))
        self.spinManPort.valueChanged.connect(lambda x: (self.cur_object.set_port(x), self.set_config_changed()))

        # module
        self.lineModuleName.textEdited.connect(lambda x: (self.cur_object.set_name(x), self.name_changed(self.cur_object), self.set_config_changed()))
        self.lineModuleIp.textEdited.connect(lambda x: (self.cur_object.set_ip(x), self.set_config_changed()))
        self.spinModulePort.valueChanged.connect(lambda x: (self.cur_object.set_port(x), self.set_config_changed()))
        # self.cboModuleGM.currentIndexChanged.connect(lambda x: (self.test(x), self.set_config_changed()))
        self.spinModuleStage.valueChanged.connect(lambda x: (isinstance(self.cur_object, bcc.SyncModule) and self.cur_object.set_stage(x), self.set_config_changed()))
        self.spinModuleBufferSize.valueChanged.connect(lambda x: (isinstance(self.cur_object, bcc.AsyncModule) and self.cur_object.set_bufferSize(x), self.set_config_changed()))

        # unit
        self.lineUnitName.textEdited.connect(lambda x: (self.cur_object.set_name(x), self.name_changed(self.cur_object), self.set_config_changed()))
        self.lineUnitClass.textEdited.connect(lambda x: (self.cur_object.set_class_(x), self.set_config_changed()))

        # stream
        self.lineStreamName.textEdited.connect(lambda x: (self.cur_object.set_name(x), self.name_changed(self.cur_object), self.set_config_changed()))
        self.spinStreamChannels.valueChanged.connect(lambda x: (self.cur_object.set_channels(x), self.set_config_changed()))
        self.spinStreamBlockSize.valueChanged.connect(lambda x: (self.cur_object.set_blocksize(x), self.set_config_changed()))
        self.spinStreamSamplingRate.valueChanged.connect(lambda x: (isinstance(self.cur_object, bcc.SignalStream) and self.cur_object.set_samplingrate(x), self.set_config_changed()))
        self.textStreamInitValue.textChanged.connect(lambda: (isinstance(self.cur_object, bcc.EventStream) and self.cur_object.set_initvalue(self.textStreamInitValue.toPlainText()), self.set_config_changed()))

        # parameter
        self.lineParamName.textEdited.connect(lambda x: (self.cur_object.set_name(x), self.name_changed(self.cur_object), self.set_config_changed()))
        self.textParamValue.textChanged.connect(lambda: (self.cur_object.set_value(self.textParamValue.toPlainText()), self.set_config_changed()))
        self.cboParamType.currentIndexChanged.connect(lambda x: (self.cur_object.set_type_(x), self.set_config_changed()))

    def name_changed(self, obj):
        sel = self.treeConfig.selectedIndexes()
        if len(sel) == 0 or not sel[0].isValid():
            return

        # first get the old name from the tree view
        old_name = self.treeConfig.model().data(sel[0], QtCore.Qt.DisplayRole)

        # next, update the data in the tree model with the new name
        self.treeConfig.model().set_content(sel[0], obj.name)

        # finally update the object owning the item
        if isinstance(obj.parent, bcc.ObjectCollection):
            del obj.parent[old_name]
            obj.parent[obj.name] = obj
        else:
            # TODO this still crashes occasionally and not sure why yet...
            print('Not an ObjectCollection', obj.parent)
            raise ValueError('uhoh')
        
    def reload_configs(self, autoshow=True):
        if not autoshow:
            self.cboConfigName.blockSignals(True)
        self.cboConfigName.clear()
        self.cboConfigName.addItems(self.get_configs())
        if not autoshow:
            self.cboConfigName.blockSignals(False)

    def set_config_changed(self, state=True):
        if state and self.windowTitle().find('unsaved') != -1:
            self.setWindowTitle(self.windowTitle() + ' (unsaved changes)')
        self.config_changed = state

    def get_configs(self):
        logger.info('Scanning for config files in %s' % CONFIG_ROOT)
        confs = []

        for f in os.listdir(CONFIG_ROOT):
            if f.lower().endswith('.conf'):
                confs.append(f)

        return confs

    def check_env(self):
        VARS = [
            'BZI_MOREGRASP',    
            'BZI_FRAMEWORK_ROOT',
            'BZI_MOREGRASP_DATA',
            'BOOST_ROOT'
        ]

        missing = []
        for var in VARS:
            if var not in os.environ:
                missing.append(var)

        if len(missing) > 0:
            message = 'Missing environment variables: \n' 
            message += '\n'.join(missing)
            QtWidgets.QMessageBox.warning(self, 'bbtgui', message)
        else:
            QtWidgets.QMessageBox.information(self, 'bbtgui', 'No missing environment variables found')

    def show_error_object(self, item):
        """
        Handles clicking on an error message, selects the corresponding tree item
        if there is one attached to the message
        """
        item_data = item.data(QtCore.Qt.UserRole)
        if item_data is not None:
            self.display_object(item_data)

    def show_context_menu(self, pos):
        # index of the item in the tree at clicked position
        index = self.treeConfig.indexAt(pos)
        if not index.isValid():
            return
        # object in the model for this item
        obj = index.data(TreeModel.ITEM_DATA_ROLE)

        # TODO create context menu depending on item type
        menu = QtWidgets.QMenu(self)

        ADD_STREAM    = 'Add stream'
        ADD_PARAMETER = 'Add parameter'
        ADD_UNIT      = 'Add unit'
        ADD_MODULE    = 'Add module'

        DELETE        = 'Delete'

        # streams can be added to:
        #   - controller inputs/outputs
        #   - any units
        if isinstance(obj.parent, bcc.Controller) or (isinstance(obj.parent, bcc.Unit) and obj.name != 'Parameters'):
            menu.addAction(QtWidgets.QAction(ADD_STREAM, menu))
        # parameters can be added to:
        #   - manager
        #   - any units
        elif (isinstance(obj.parent, bcc.Manager) and obj.name == 'Parameters') or (isinstance(obj.parent, bcc.Unit) and obj.name == 'Parameters'):
            menu.addAction(QtWidgets.QAction(ADD_PARAMETER, menu))
        # units can be added to:
        #   - manager
        #   - any module
        elif (isinstance(obj.parent, bcc.Manager) and obj.name == 'Units') or (isinstance(obj, bcc.Module)):
            menu.addAction(QtWidgets.QAction(ADD_UNIT, menu))
        # modules can be added to:
        #   - modules list
        elif isinstance(obj.parent, bcc.Configuration) and obj.name == 'Modules':
            menu.addAction(QtWidgets.QAction(ADD_MODULE, menu))

        # if the object is one of: Unit, Parameter, Module, Stream then allow deletion (excluding Manager)
        if isinstance(obj, bcc.Stream) or isinstance(obj, bcc.Unit) or isinstance(obj, bcc.BaseParameter) or \
                (isinstance(obj, bcc.Module) and not isinstance(obj, bcc.Manager)):
            menu.addAction(QtWidgets.QAction('%s %s' % (DELETE, obj.name), menu))

        # display context menu at click position and get response
        action = menu.exec_(self.treeConfig.mapToGlobal(pos))
        if action is None:
            return

        if action.text() == ADD_STREAM:
            self.add_stream(obj)
        elif action.text() == ADD_PARAMETER:
            self.add_parameter(obj)
        elif action.text() == ADD_UNIT:
            self.add_unit(obj)
        elif action.text() == ADD_MODULE:
            self.add_module(obj)
        elif action.text().startswith(DELETE):
            self.remove_object(obj)

    def exit(self):
        QtCore.QCoreApplication.exit()

    def remove_object(self, obj):
        logger.debug('Removing object %s (type=%s)' % (obj.name, type(obj)))
        del obj.parent[obj.name]
        self.display_object(obj.parent)
        self.reload_tree()

    def add_object(self, parent, obj):
        logger.debug('Adding object %s (type=%s)' % (obj.name, type(obj)))
        obj.parent = parent
        self.display_object(parent)
        self.reload_tree()

    def add_module_dialog(self, parent):
        dialog = BbtGuiAddModuleDialog(parent, self.config.next_free_port(), self.config.next_free_stage(), self)
        result = dialog.exec_()
        if result != QtWidgets.QDialog.Accepted:
            return None

        return dialog.module

    def add_module(self, parent):
        module = self.add_module_dialog(parent)
        if module is None:
            return

        parent[module.name] = module
        self.add_object(parent, module)

    def add_unit_dialog(self, parent):
        dialog = BbtGuiAddUnitDialog(parent, self)
        result = dialog.exec_()
        if result != QtWidgets.QDialog.Accepted:
            return None

        return dialog.unit

    def add_unit(self, parent):
        unit = self.add_unit_dialog(parent)
        if unit is None:
            return

        if isinstance(parent, bcc.ObjectCollection):
            parent[unit.name] = unit
            self.add_object(parent, unit)
        else:
            parent.units[unit.name] = unit
            self.add_object(parent.units, unit)

    def add_parameter_dialog(self, parent, is_init):
        dialog = BbtGuiAddParameterDialog(parent, is_init, self)
        result = dialog.exec_()
        if result != QtWidgets.QDialog.Accepted:
            return None

        return dialog.parameter

    def add_parameter(self, parent):
        # init parameters are those with parents that are streams
        is_init = False
        if isinstance(parent, bcc.Stream):
            is_init = True
        parameter = self.add_parameter_dialog(parent, is_init)
        if parameter is None:
            return

        parent[parameter.name] = parameter
        self.add_object(parent, parameter)

    def add_stream_dialog(self, to_object):
        dialog = BbtGuiAddStreamDialog(to_object, self)
        result = dialog.exec_()
        if result != QtWidgets.QDialog.Accepted:
            return None

        return dialog.stream

    def add_stream(self, parent):
        stream = self.add_stream_dialog(parent)
        if stream is None:
            return

        parent[stream.name] = stream
        self.add_object(parent, stream)

    def unit_stream_selected(self, int_index):
        self.display_object(self.cboUnitStreams.itemData(int_index))
        self.tabEdit.setCurrentIndex(BbtGui.TAB_STREAM)

    def unit_parameter_selected(self, int_index):
        self.display_object(self.cboUnitParameters.itemData(int_index))
        self.tabEdit.setCurrentIndex(BbtGui.TAB_PARAMETER)

    def update_statusbar(self):
        msg = '[%s] modules=%d, units=%d' % (os.path.basename(self.config.filename), len(self.config.modules), len(self.config.units()))
        self.statusbar.showMessage(msg)

    def save_config_as(self):
        if not self.config:
            return

        name = QtWidgets.QFileDialog.getExistingDirectory(self, 'Save configuration to folder', self.config.basepath())
        if not name:
            return
        self.save_config(name)
        self.set_config_changed(False)

    def save_config(self, path):
        if not self.config:
            return
        if not self.verify_config():
            if QtWidgets.QMessageBox.question(self, 'Save configuration anyway?', 'Errors were detected in the configuration. Do you want to save it anyway?') != QtWidgets.QMessageBox.Yes:
                return

        if len(self.config.unit_locations()) > 1:
            if QtWidgets.QMessageBox.question(self, 'Update unit locations?', 'The XML files for the units in this configuration appear to be stored in multiple different folders. Do you want to update the locations so that they are all placed in the same folder as the .conf file?') == QtWidgets.QMessageBox.Yes:
                self.config.write(os.path.join(self.config.basepath(), self.config.name()))
                self.set_config_changed(False)
                QtWidgets.QMessageBox.information(self, 'Unit locations updated', 'All units for this configuration have been saved into the folder %s' % (os.path.join(self.config.basepath(), self.config.name())))
                return

        self.config.write()
        self.set_config_changed(False)

    def new_config(self):
        name, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Set new configuration filename', self.config.basepath(), 'Conf files (*.conf)')
        if not name:
            return

        # TODO
        self.config = self.default_config(name)
        # stop the event handler from firing while we repopulate the list
        self.cboConfigName.blockSignals(True)
        self.reload_configs()
        self.load_model(name)
        self.cboConfigName.setCurrentText(os.path.split(self.config.filename)[1])
        self.cboConfigName.blockSignals(False)

    def load_config(self):
        global CONFIG_ROOT
        name, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Select a configuration file', CONFIG_ROOT, 'Conf files (*.conf)')
        if not name:
            return

        CONFIG_ROOT = os.path.dirname(name)
        logger.info('CONFIG_ROOT is now %s' % CONFIG_ROOT)
        # stop the event handler from firing while we repopulate the list
        self.cboConfigName.blockSignals(True)
        self.reload_configs()
        self.load_model(name)
        self.cboConfigName.setCurrentText(os.path.split(self.config.filename)[1])
        self.cboConfigName.blockSignals(False)

    def verify_config(self):
        """
        Check that the current configuration seems valid
        """

        self.listMessages.clear()

        errors, warnings = self.config.validate()
        i = 1
        for msg, obj in errors:
            item = QtWidgets.QListWidgetItem('%d. %s' % (i, msg), self.listMessages)
            if obj is not None:
                item.setData(QtCore.Qt.UserRole, obj)
            self.listMessages.addItem(item)
            i += 1

        for msg, obj in warnings:
            item = QtWidgets.QListWidgetItem('%d. [WARNING] %s' % (i, msg), self.listMessages)
            if obj is not None:
                item.setData(QtCore.Qt.UserRole, obj)
            self.listMessages.addItem(item)
            i += 1
        return (len(errors) == 0)

    def default_module(self):
        """
        Defines a default module to be attached to empty configs
        """
        return bcc.SyncModule('module1', '127.0.0.1', '1951', bcc.BZI_MODULE_CPP, 1, None, self.config.modules)

    def default_config(self, name):
        return bcc.Configuration.create(name)

    def load_model(self, filename):
        if filename is None:
            self.config = bcc.Configuration(None)
            self.tabEdit.setEnabled(False)
            return

        logger.info('Load model: %s' % filename)
        self.tabEdit.setEnabled(True)
        try:
            self.config = bcc.Configuration(os.path.abspath(filename))
        except Exception as e:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Failed to load %s (%s)' % (filename, str(e)))
            return 

        if len(self.config.modules) == 0:
            mod = self.default_module()
            self.config.modules[mod.name] = mod

        self.reload_tree()

    def reload_tree(self):
        """
        Reload the model and tree view when the structure of the config is changed
        """

        self.configModel = TreeModel(self.config)
        self.treeConfig.setModel(self.configModel)
        self.treeConfig.expandAll()

        # have to reregister the callback here as model changed
        self.treeConfig.selectionModel().selectionChanged.connect(self.tree_selection_changed)
        self.tabEdit.setCurrentIndex(BbtGui.TAB_CONTROLLER)
        self.update_statusbar()
        self.btnSave.setEnabled(True)
        self.btnSaveAs.setEnabled(True)

        self.lblConInputStreams.setText('%d' % len(self.config.controller.inputs))
        self.lblConOutputStreams.setText('%d' % len(self.config.controller.outputs))

        self.lblManUnits.setText('%d' % len(self.config.manager.units))
        self.lblManParameters.setText('%d' % len(self.config.manager.parameters))

        self.display_object(self.config.controller)
        self.tabEdit.setCurrentIndex(BbtGui.TAB_CONTROLLER)

    def display_stream(self, obj):
        self.tabEdit.setCurrentIndex(BbtGui.TAB_STREAM)
        self.lineStreamName.setText(obj.name)
        self.spinStreamChannels.setValue(int(obj.channels))
        self.spinStreamBlockSize.setValue(int(obj.blocksize))
        if isinstance(obj, bcc.EventStream):
            self.textStreamInitValue.setEnabled(True)
            self.textStreamInitValue.setPlainText(obj.initvalue)
            self.cboStreamType.setCurrentIndex(BbtGui.STREAM_EVENT)
            self.spinStreamSamplingRate.setEnabled(False)
        else:
            self.textStreamInitValue.setEnabled(False)
            self.spinStreamSamplingRate.setValue(int(obj.samplingrate))
            self.spinStreamSamplingRate.setEnabled(True)
            self.cboStreamType.setCurrentIndex(BbtGui.STREAM_SIGNAL)

        self.cur_stream = obj

    def display_parameter(self, obj):
        self.tabEdit.setCurrentIndex(BbtGui.TAB_PARAMETER)
        self.lineParamName.setText(obj.name)
        self.cboParamType.setCurrentIndex(obj.type_)
        self.textParamValue.setPlainText(obj.value)

        self.cur_parameter = obj

    def display_manager(self, obj):
        self.tabEdit.setCurrentIndex(BbtGui.TAB_MANAGER)
        self.lineManName.setText(obj.name)
        self.lineManIp.setText(obj.ip)
        self.spinManPort.setValue(int(obj.port))

    def display_unit(self, obj):
        self.tabEdit.setCurrentIndex(BbtGui.TAB_UNIT)
        self.lineUnitName.setText(obj.name)
        self.lineUnitClass.setText(obj.class_)
        self.lblUnitXmlPath.setText(obj.xmlpath)
        self.cboUnitType.setCurrentIndex(bcc.Unit.lookup(obj.__class__.__name__))
        self.cboUnitParameters.clear()
        self.cboUnitStreams.clear()
        for name, param in iteritems(obj.parameters):
            self.cboUnitParameters.addItem('%s = %s' % (name, param.value), param)
        for name, stream in iteritems(obj.inputs):
            self.cboUnitStreams.addItem('[input] %s' % name, stream)
        for name, stream in iteritems(obj.outputs):
            self.cboUnitStreams.addItem('[output] %s' % name, stream)

        self.cur_unit = obj

    def display_module(self, obj):
        self.tabEdit.setCurrentIndex(BbtGui.TAB_MODULE)
        self.lineModuleName.setText(obj.name)
        self.lineModuleIp.setText(obj.ip)
        self.spinModulePort.setValue(int(obj.port))
        if isinstance(obj, bcc.SyncModule):
            self.cboModuleSyncAsync.setCurrentIndex(BbtGui.MOD_SYNC)
            self.spinModuleStage.setEnabled(True)
            self.spinModuleStage.setVisible(True)
            self.lblModuleStage.setVisible(True)
            self.spinModuleStage.setValue(obj.stage)
            self.spinModuleBufferSize.setVisible(False)
            self.spinModuleBufferSize.setEnabled(False)
            self.lblModuleBufferSize.setVisible(False)
        else:
            self.cboModuleSyncAsync.setCurrentIndex(BbtGui.MOD_ASYNC)
            self.spinModuleStage.setEnabled(False)
            self.spinModuleStage.setVisible(False)
            self.lblModuleStage.setVisible(False)
            self.spinModuleBufferSize.setEnabled(True)
            self.spinModuleBufferSize.setVisible(True)
            self.lblModuleBufferSize.setVisible(True)
        if obj.exe == bcc.BZI_MODULE_CPP:
            self.cboModuleGM.setCurrentIndex(BbtGui.MOD_CPP)
        else:
            self.cboModuleGM.setCurrentIndex(BbtGui.MOD_MATLAB)

        self.cur_module = obj

    def display_controller(self, obj=None):
        obj = obj or self.config.controller

        self.tabEdit.setCurrentIndex(BbtGui.TAB_CONTROLLER)
        self.lineConName.setText(obj.name)
        self.spinConNumRetries.setValue(int(obj.numRetries))

    def get_tree_parent_object(self, index):
        parent_object = None
        parent_index = index
        while parent_object is None:
            parent_index = self.configModel.parent(parent_index)
            parent_object = self.configModel.data(parent_index, TreeModel.ITEM_DATA_ROLE)

        return parent_object

    def display_object(self, obj, index=None):
        if self.cur_object is not None:
            self.cur_object = None

        if obj is None:
            self.tabEdit.setCurrentIndex(BbtGui.TAB_NOTHING)
            return

        self.cur_object = obj
        logger.debug('Current object: %s (type=%s)' % (obj.name, type(obj)))

        if isinstance(obj, bcc.Controller):
            self.display_controller(obj)
        elif isinstance(obj, bcc.Manager):
            self.display_manager(obj)
        elif isinstance(obj, bcc.Module):
            self.display_module(obj)
        elif isinstance(obj, bcc.Unit):
            self.display_unit(obj)
        elif isinstance(obj, bcc.Stream):
            self.display_stream(obj)
        elif isinstance(obj, bcc.BaseParameter):
            self.display_parameter(obj)
        else:
            self.tabEdit.setCurrentIndex(BbtGui.TAB_NOTHING)

    def tree_clicked(self, index):
        if not index.isValid():
            return

        obj = self.configModel.data(index, TreeModel.ITEM_DATA_ROLE)
        self.display_object(obj, index)

    @QtCore.pyqtSlot(QtCore.QItemSelection, QtCore.QItemSelection)
    def tree_selection_changed(self, now, was):
        if len(now.indexes()) == 0:
            return

        obj = self.configModel.data(now.indexes()[0], TreeModel.ITEM_DATA_ROLE)
        self.display_object(obj, now.indexes()[0])

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = BbtGui()
    window.show()
    sys.exit(app.exec_())
