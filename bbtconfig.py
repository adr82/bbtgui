import sys, os, logging, re
import lxml.etree as ET
from collections import MutableMapping
from six import iteritems, string_types, itervalues

logger = logging.getLogger('bbtconfig')
logger.setLevel(logging.INFO)
logger.propagate = False
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter('%(module)s.%(funcName)s:%(lineno)d  %(message)s'))
logger.addHandler(handler)

# enumeration of Bzi types
[BZI_INT, BZI_INT_VEC, BZI_STRING, BZI_STRING_VEC, BZI_DOUBLE, BZI_DOUBLE_VEC, BZI_BOOL, BZI_BOOL_VEC] = list(range(8))
BZI_TYPE_MIN = 0
BZI_TYPE_MAX = BZI_BOOL_VEC

# mapping of IDs to the actual names
BZI_TYPES = \
    {
        BZI_INT        : 'Bzi::BziInt',
        BZI_INT_VEC    : 'Bzi::BziIntVector',
    
        BZI_STRING     : 'Bzi::BziString',
        BZI_STRING_VEC : 'Bzi::BziStringVector',

        BZI_DOUBLE     : 'Bzi::BziDouble',
        BZI_DOUBLE_VEC : 'Bzi::BziDoubleVector',

        BZI_BOOL       : 'Bzi::BziBool',
        BZI_BOOL_VEC   : 'Bzi::BziBoolVector'
    }

# defined unit types
[BZI_UNIT_GENERIC, BZI_UNIT_MATLAB, BZI_UNIT_DISPLAY] = list(range(3))
BZI_UNIT_MAX = BZI_UNIT_DISPLAY
BZI_UNITS = \
    {
        BZI_UNIT_GENERIC : 'GenericUnit',
        BZI_UNIT_MATLAB  : 'MatlabUnit',
        BZI_UNIT_DISPLAY : 'DisplayUnit'
        
    }

# defined module types
BZI_MODULE_ASYNC = 'online_asynchronous'
BZI_MODULE_SYNC = 'online_synchronous'

# defined module executables
BZI_MODULE_CPP = 'bin/bziModule'
BZI_MODULE_MATLAB = 'bin/bziModule_matlab'

# used to generate getters and setters for various classes (mainly so that
# the attributes can be easily updated by lambdas in the graphical interface)
def add_gets_and_sets(obj, attrs):
    for attr in attrs:
        obj.__setattr__('get_' + attr, lambda attr=attr : obj.__getattribute__(attr))
        obj.__setattr__('set_' + attr, lambda x, attr=attr : obj.__setattr__(attr, x))

def checkattr(root, name, allow_empty=False):
    if not allow_empty and name not in root.attrib:
        raise Exception('Missing attribute "%s" on element %s\n%s' % (name, root.tag, ET.tostring(root, pretty_print=True)))
    return root.attrib[name]

class Types(object):
    """
    Utility functions for dealing with Bzi types
    """

    @staticmethod
    def lookup(t_str):
        for k, v in iteritems(BZI_TYPES):
            if t_str == v:
                return k
        raise ValueError('Unknown type (%s)' % t_str)

class ObjectCollection(MutableMapping):
    """
    Thin wrapper around a dict which just adds a name and a reference
    to an owning object for the items in the dict.
    """

    def __init__(self, parent, name):
        self.objects = {}
        self.parent = parent
        self.name = name

        add_gets_and_sets(self, ['name'])

    def __getitem__(self, key):
        return self.objects[key]

    def __setitem__(self, key, value):
        self.objects[key] = value

    def __delitem__(self, key):
        del self.objects[key]

    def __iter__(self):
        return iter(self.objects)

    def __len__(self):
        return len(self.objects)

    def __repr__(self):
        s = '[%s (%s <== %s)\n' % (self.__class__.__name__, self.name, self.parent)
        s += '  -> contains %d objects' % (len(self.objects))
        return s

class Stream(object):
    """
    Base class for stream objects (signals and events)
    """

    INPUT = 'input'
    OUTPUT = 'output'

    def __init__(self, name, channels, blocksize, parent):
        self.name = name
        self.channels = channels
        self.blocksize = blocksize
        self.parent = parent

        add_gets_and_sets(self, ['name', 'channels', 'blocksize'])

    def xml(self, root, direction):
        stream = ET.Element(direction, name=self.name)
        stream.attrib['type'] = 'Bzi::%s' % self.__class__.__name__
        # TODO handle same way as other classes?
        attrs = ['channels', 'blockSize', 'samplingRate', 'initValue']
        for attr in attrs:
            if hasattr(self, attr.lower()):
                stream.append(ET.Element('initparameter', name=attr, value=str(getattr(self, attr.lower()))))
        return stream

    def match(self, other):
        """
        Returns true if the other stream matches all basic parameters of this one
        """
        return (self.name == other.name and self.channels == other.channels and self.blocksize == other.blocksize)

class StreamFactory(object):
    EVENT_STREAM = 'Bzi::EventStream'
    SIGNAL_STREAM = 'Bzi::SignalStream'

    @staticmethod
    def parse(parent, root):
        """
        Create the appropriate type of Stream object from the XML defintion
        """
        type_ = checkattr(root, 'type')
        if type_ == StreamFactory.EVENT_STREAM:
            return EventStream.parse(parent, root)
        elif type_ == StreamFactory.SIGNAL_STREAM:
            return SignalStream.parse(parent, root)

        raise ValueError('Invalid stream type (%s)' % (type_))
    
class SignalStream(Stream):
    """
    Represents a SignalStream
    """

    def __init__(self, name, channels, blocksize, parent, samplingrate):
        Stream.__init__(self, name, channels, blocksize, parent)

        self.samplingrate = samplingrate
        add_gets_and_sets(self, ['samplingrate'])

    def match(self, other):
        """
        Returns true if the other object is a SignalStream and all parameters match
        """
        if not isinstance(other, SignalStream):
            return False

        return (self.samplingrate == other.samplingrate) and Stream.match(self, other)

    @classmethod
    def parse(cls, parent, root):
        """
        Create a SignalStream instance from an XML definition
        """
        name = checkattr(root, 'name')
        iparams = {}
        for p in ['channels', 'blockSize', 'samplingRate']:
            iparams[p] =  root.find('initparameter[@name="%s"]' % p)
            if iparams[p] is None:
                raise Exception('Missing "%s" InitParameter for signal stream "%s"' % (p, name))
            iparams[p] = InitParameter.parse(iparams[p])
        return cls(name, iparams['channels'].value, iparams['blockSize'].value, parent, iparams['samplingRate'].value)

class EventStream(Stream):
    """
    Represents an EventStream
    """

    def __init__(self, name, channels, blocksize, parent, initvalue):
        Stream.__init__(self, name, channels, blocksize, parent)

        self.initvalue = initvalue
        add_gets_and_sets(self, ['initvalue'])

    def match(self, other):
        """
        Returns true if the other object is an EventStream and all parameters match
        """
        if not isinstance(other, EventStream):
            return False

        return (self.initvalue == other.initvalue) and Stream.match(self, other)

    @classmethod
    def parse(cls, parent, root):
        """
        Create an EventStream instance from an XML definition
        """
        name = checkattr(root, 'name')
        iparams = {}
        for p in ['channels', 'blockSize', 'initValue']:
            iparams[p] = root.find('initparameter[@name="%s"]' % p)
            if iparams[p] is None:
                raise Exception('Missing "%s" InitParameter for signal stream "%s"' % (p, name))
            iparams[p] = InitParameter.parse(iparams[p])
        return cls(name, iparams['channels'].value, iparams['blockSize'].value, parent, iparams['initValue'].value)

class ParameterFactory(object):

    @staticmethod
    def parse(root):
        """
        Create the appropriate type of parameter object from an XML definition
        """
        if root.tag == 'initparameter':
            return InitParameter.parse(root)
        elif root.tag == 'parameter':
            return Parameter.parse(root)

        raise ValueError('Unknown parameter type (%s)' % root.tag)

class BaseParameter(object):
    """
    Base class for parameters
    """

    def __init__(self, name, value, parent=None):
        self.name = name
        self.value = value
        self.type_ = None
        self.parent = parent

        add_gets_and_sets(self, ['name', 'value', 'type_'])

    @staticmethod
    def extract(root):
        name = checkattr(root, 'name')
        value = checkattr(root, 'value')
        return (name, value)

    def xml(self, root):
        param = ET.Element(self.__class__.__name__.lower(), name=self.name, value=str(self.value))
        param.attrib['type'] = BZI_TYPES[self.type_]
        return param

class InitParameter(BaseParameter):
    """
    Init parameters are parameters that can only have type INT or INT_VEC...
    """

    def __init__(self, name, value):
        BaseParameter.__init__(self, name, value)
        self.type_ = self.get_type(value)

    def get_type(self, val):
        if val.find('[') == -1:
            return BZI_INT
        return BZI_INT_VEC

    @classmethod
    def parse(cls, root):
        """
        Create an InitParameter instance from an XML definition
        """
        param = cls(*BaseParameter.extract(root))
        return param

class Parameter(BaseParameter):
    """
    Represents a Parameter
    """
    def __init__(self, name, value, type_, parent=None):
        BaseParameter.__init__(self, name, value, parent)
        self.type_ = type_

    @classmethod
    def parse(cls, root):
        """
        Create a Parameter instance from an XML definition
        """
        (name, value) = BaseParameter.extract(root)
        type_ = Types.lookup(checkattr(root, 'type'))
        obj = cls(name, value, type_)
        return obj

    def _validate_array(self, arr, func):
        if arr.find('[') == -1 or arr.find(']') == -1:
            return False
        # strip off the [] chars, collapse whitespace into single spaces and
        # then split around those remaining spaces
        vals = arr[arr.index('[')+1:arr.rindex(']')]
        vals = re.sub('\s+', ' ', vals)
        vals = vals.split(' ')
        for v in vals:
            if len(v) > 0 and not self._validate_value(v.strip(), func):
                return False

        return True

    def _validate_value(self, val, func):
        try:
            func(val)
        except ValueError:
            return False

        return True

    def _validate_bool(self, val):
        if not isinstance(val, string_types):
            raise ValueError('_validate_bool')
        if val.lower() != 'true' and val.lower() != 'false':
            raise ValueError('_validate_bool')

    def validate(self):
        if self.type_ == BZI_INT:
            return self._validate_value(self.value, int)
        elif self.type_ == BZI_INT_VEC:
            return self._validate_array(self.value, int)
        elif self.type_ == BZI_STRING:
            return self._validate_value(self.value, str)
        elif self.type_ == BZI_STRING_VEC:
            return self._validate_array(self.value, str)
        elif self.type_ == BZI_DOUBLE:
            return self._validate_value(self.value, float)
        elif self.type_ == BZI_DOUBLE_VEC:
            return self._validate_array(self.value, float)
        elif self.type_ == BZI_BOOL:
            return self._validate_value(self.value, self._validate_bool)
        elif self.type_ == BZI_BOOL_VEC:
            return self._validate_array(self.value, self._validate_bool)
        else:
            raise ValueError('Parameter has unknown type %s' % (self.type_))
    
class UnitFactory(object):

    @staticmethod
    def parse(module, root, basepath):
        """
        Creates the appropriate type of Unit subclass object given the XML definition
        and parent module
        """
        type_ = checkattr(root, 'type')
        if type_ == BZI_UNITS[BZI_UNIT_GENERIC]:
            return GenericUnit.parse(module, root, basepath)
        elif type_ == BZI_UNITS[BZI_UNIT_MATLAB]:
            return MatlabUnit.parse(module, root, basepath)
        elif type_ == BZI_UNITS[BZI_UNIT_DISPLAY]:
            return DisplayUnit.parse(module, root, basepath)
    
        raise ValueError('Unknown unit type (%s)' % type_)

class Unit(object):
    """
    Base class for Units
    """
    def __init__(self, name, class_, xmlpath, parent):
        self.name = name
        self.class_ = class_
        self.xmlpath = xmlpath
        self.inputs = ObjectCollection(self, 'Inputs')
        self.outputs = ObjectCollection(self, 'Outputs')
        self.parameters = ObjectCollection(self, 'Parameters')
        self.parent = parent

        add_gets_and_sets(self, ['name', 'class_', 'xmlpath'])

    @staticmethod
    def lookup(t_str):
        for k, v in iteritems(BZI_UNITS):
            if t_str == v:
                return k
        raise ValueError('Unknown type (%s)' % t_str)

    def __repr__(self):
        s = '[%s (%s, %s)\n' % (self.__class__.__name__, self.name, self.class_)
        s += '  -> %s\n' % (self.xmlpath)
        return s

    def xml(self, root):
        unit = ET.Element('unit', name=self.name, xml=self.xmlpath)
        unit.attrib['class'] = self.class_
        unit.attrib['type'] = self.__class__.__name__
        return unit

    def write(self):
        root = self.xml(None)
        root.attrib['xml'] = ''
        for p in itervalues(self.parameters):
            root.append(p.xml(root))

        for s in itervalues(self.inputs):
            root.append(s.xml(root, Stream.INPUT))
        for s in itervalues(self.outputs):
            root.append(s.xml(root, Stream.OUTPUT))

        with open(self.xmlpath, 'wb') as f:
            tree = ET.ElementTree(root)
            tree.write(f, pretty_print=True, xml_declaration=True, encoding="UTF-8")

    @staticmethod
    def extract(parent, root, unitpath):
        """
        Utility method to extract common properties for all Units from XML definition
        """
        name = checkattr(root, 'name')
        class_ = checkattr(root, 'class')
        return (name, class_, unitpath, parent)

    def load(self):
        """
        Parse the XML file defined for the current unit
        """
        logger.info('Unit loading from: %s' % self.xmlpath)
        if not os.path.exists(self.xmlpath):
            raise OSError('Failed to load unit XML (%s)' % self.xmlpath)

        root = ET.parse(self.xmlpath).getroot()
        if root.tag != 'unit':
            raise ValueError('Unexpected root element %s in unit XML %s' % (root.tag, self.xmlpath))

        for p in root.findall('parameter'):
            parameter = ParameterFactory.parse(p)
            parameter.parent = self.parameters
            self.parameters[parameter.name] = parameter

        for i in root.findall('input'):
            input = StreamFactory.parse(self.inputs, i)
            self.inputs[input.name] = input

        for o in root.findall('output'):
            output = StreamFactory.parse(self.outputs, o)
            self.outputs[output.name] = output

class GenericUnit(Unit):
    """
    Represents a GenericUnit
    """
    def __init__(self, name, class_, xmlpath, parent):
        Unit.__init__(self, name, class_, xmlpath, parent)

    @classmethod
    def parse(cls, parent, root, basepath):
        """
        Create a GenericUnit instance from the XML definition
        """
        data = Unit.extract(parent, root, basepath)
        unit = cls(*data)
        unit.load()
        return unit

class DisplayUnit(Unit):
    """
    Represents a DisplayUnit
    """
    def __init__(self, name, class_, xmlpath, parent):
        Unit.__init__(self, name, class_, xmlpath, parent)

    @classmethod
    def parse(cls, parent, root, basepath):
        """
        Create a DisplayUnit instance from the XML definition
        """
        data = Unit.extract(parent, root, basepath)
        unit = cls(*data)
        unit.load()
        return unit

class MatlabUnit(Unit):
    """
    Represents a MatlabUnit
    """
    def __init__(self, name, class_, xmlpath, parent):
        Unit.__init__(self, name, class_, xmlpath, parent)

    @classmethod
    def parse(cls, parent, root, basepath):
        """
        Create a MatlabUnit instance from the XML definition
        """
        data = Unit.extract(parent, root, basepath)
        unit = cls(*data)
        unit.load()
        return unit

class ModuleFactory(object):

    @staticmethod
    def parse(parent, root):
        """
        Create the appropriate type of Module instance from an XML definition
        """
        operation = checkattr(root, 'operation')
        if operation == BZI_MODULE_SYNC:
            return SyncModule.parse(parent, root)
        elif operation == BZI_MODULE_ASYNC:
            return AsyncModule.parse(parent, root)

        raise ValueError('Unknown operation type (%s)' % operation)

class Module(object):
    """
    Base class for all modules
    """
    def __init__(self, name, ip, port, exe, units=None, parent=None):
        self.name = name 
        self.ip = ip 
        self.port = int(port)
        self.exe = exe
        if units is None:
            self.units = ObjectCollection(self, 'Units')
        else:
            self.units = units
        self.parent = parent

        add_gets_and_sets(self, ['name', 'ip', 'port', 'exe'])

    def __repr__(self):
        s =  ' %s: %s @ %s:%s\n' % (self.__class__.__name__, self.name, self.ip, self.port)
        s += '        exe: %s\n' % self.exe
        s += '        # units: %d\n' % len(self.units)
        s += '        Unit list:\n'
        for u in itervalues(self.units):
            s += '          %s [%s, %s, %s]\n' % (u.name, u.class_, u.__class__.__name__, u.xmlpath)

        return s

    def xml(self, root):
        mod = ET.Element('module', name=self.name, ip=self.ip, port=str(self.port), exe=self.exe, operation=self.operation)
        if isinstance(self, SyncModule):
            mod.attrib['stage'] = str(self.stage)
        if isinstance(self, AsyncModule):
            mod.attrib['bufferSize'] = str(self.buffersize)
        for u in itervalues(self.units):
            mod.append(u.xml(mod))
        return mod

    @staticmethod
    def extract(root):
        name = checkattr(root, 'name')
        ip = checkattr(root, 'ip')
        port = int(checkattr(root, 'port'))
        exe = checkattr(root, 'exe')
        return (name, ip, port, exe)

    def load_units(self, root, basepath):
        self.units = ObjectCollection(self, 'Units')
        for u in root.findall('unit'):
            # path to unit's XML file may be relative or absolute. if it's 
            # absolute just use it, otherwise prepend the path of the config
            # file to whatever is in the XML attribute
            xmlpath = checkattr(u, 'xml')
            if not os.path.isabs(xmlpath):
                xmlpath = os.path.join(basepath, xmlpath)
                logger.info('Converted relative xml path: %s => %s' % (u.attrib['xml'], xmlpath))
            unit = UnitFactory.parse(self.units, u, xmlpath)
            self.units[unit.name] = unit


class AsyncModule(Module):
    """
    Represents an asynchronous module
    """
    def __init__(self, name, ip, port, exe, buffersize=None, units=None, parent=None):
        Module.__init__(self, name, ip, port, exe, units, parent)
        self.buffersize = buffersize 
        self.operation = BZI_MODULE_ASYNC
        add_gets_and_sets(self, ['buffersize'])

    def __repr__(self):
        s = Module.__repr__(self)
        s += '              --> bufferSize: %s\n' % self.buffersize
        return s

    @classmethod
    def parse(cls, parent, root):
        """
        Create an AsyncModule instance from the XML definition
        """
        module = cls(*Module.extract(root))
        module.parent = parent.modules
        module.buffersize = int(checkattr(root, 'bufferSize'))
        module.load_units(root, parent.basepath())
        return module

class SyncModule(Module):
    """
    Represents a synchronous module
    """
    def __init__(self, name, ip, port, exe, stage=None, units=None, parent=None):
        Module.__init__(self, name, ip, port, exe, units, parent)
        self.stage = stage
        self.operation = BZI_MODULE_SYNC
        add_gets_and_sets(self, ['stage'])

    def __repr__(self):
        s = Module.__repr__(self)
        s += '             --> stage: %s\n' % self.stage
        return s

    @classmethod
    def parse(cls, parent, root):
        """
        Create a SyncModule instance from an XML definition
        """
        module = cls(*Module.extract(root))
        module.parent = parent.modules
        module.stage = int(checkattr(root, 'stage'))
        module.load_units(root, parent.basepath())
        return module

class Manager(Module):
    """
    Represents a Manager instance (special type of Module)
    """
    def __init__(self, name, ip, port, exe, units=None):
        Module.__init__(self, name, ip, port, exe, units)
        self.parameters = ObjectCollection(self, 'Parameters')

    def __repr__(self):
        s = Module.__repr__(self)
        s += '   # parameters: %d\n' % len(self.parameters)
        for p in itervalues(self.parameters):
            s += '     %s [%s = %s]\n' % (p.name, BZI_TYPES[p.type_], p.value)
        return s

    def xml(self, root):
        manager = ET.Element('manager', name=self.name, ip=self.ip, port=str(self.port), exe=self.exe)
        for u in itervalues(self.units):
            manager.append(u.xml(manager))
        for p in itervalues(self.parameters):
            manager.append(p.xml(manager))
        return manager

    def add_softcycle(self):
        soft_cycle = Parameter('softCycle', '31.25', BZI_DOUBLE, self.parameters)
        self.parameters[soft_cycle.name] = soft_cycle

    @classmethod
    def parse(cls, config, root):
        """
        Create a Manager instance from an XML definition
        """
        logger.info('Parsing Manager from XML')

        manager = cls(*Module.extract(root))
        manager.parent = config
        manager.load_units(root, config.basepath())

        parameters = root.findall('parameter')
        logger.info('Found %d parameters in manager' % len(parameters))
        for p in parameters:
            param = Parameter.parse(p)
            param.parent = manager.parameters
            manager.parameters[param.name] = param

        return manager

class Controller(object):
    """
    Represents a Controller instance
    """
    def __init__(self, config, name=None, numRetries=5):
        self.config = config
        self.name = name or "Controller"
        self.numRetries = numRetries
        self.inputs = ObjectCollection(self, 'inputs')
        self.outputs = ObjectCollection(self, 'outputs')
        self.parent = config

        add_gets_and_sets(self, ['name', 'numRetries'])

    def __repr__(self):
        s =  ' Controller: %s\n' % self.name
        s += '   # numRetries = %d\n' % self.numRetries
        s += '   # inputs = %d\n' % len(self.inputs)
        s += '   Input stream list:\n'
        for i in itervalues(self.inputs):
            s += '      %s [%s]\n' % (i.name, i.__class__.__name__)
        s += '   # outputs = %d\n' % len(self.outputs)
        s += '   Output stream list:\n'
        for i in itervalues(self.outputs):
            s += '      %s [%s]\n' % (i.name, i.__class__.__name__)
        return s

    def xml(self, root):
        ele = ET.Element('controller', name=self.name, netConnectionTries=str(self.numRetries))
        for s in itervalues(self.inputs):
            ele.append(s.xml(ele, Stream.INPUT))
        for s in itervalues(self.outputs):
            ele.append(s.xml(ele, Stream.OUTPUT))

        return ele

    @classmethod
    def parse(cls, config, root):
        """
        Create a Controller instance from an XML definition
        """
        logger.info('Parsing Controller from XML')
    
        name = checkattr(root, 'name')
        tries = checkattr(root, 'netConnectionTries')
        controller = cls(config, name, int(tries))

        all_streams = root.findall('.//input')
        all_streams.extend(root.findall('.//output'))
        logger.info('Found %d total streams' % len(all_streams))
        for stream in all_streams:
            logger.info('Parsing stream...')
            if stream.tag == 'input':
                stream_obj = StreamFactory.parse(controller.inputs, stream)
            else:
                stream_obj = StreamFactory.parse(controller.outputs, stream)

            if stream.tag == 'input':
                controller.inputs[stream_obj.name] = stream_obj
            elif stream.tag == 'output':
                controller.outputs[stream_obj.name] = stream_obj
            else:
                raise ValueError('Unknown stream tag (%s)' % (stream.tag))

        return controller

class Configuration(object):
    """
    Represents the overall configuration
    """
    def __init__(self, filename):
        if filename is None or len(filename) == 0:
            logger.error('No filename supplied')
            raise ValueError('Invalid filename')

        self.modules = ObjectCollection(self, 'Modules')
        self.filename = None # filename of the .conf file 
            
        if os.path.exists(filename):
            logger.info('Loading configuration: %s' % filename)
            filename = os.path.abspath(filename)
            etree = ET.parse(filename)
            root = etree.getroot()
            if root.tag != 'bziconfigurations':
                raise ValueError('XML root element not recognised! (should be "bziconfigurations")')
            self.filename = filename
            self.parse(root)
        else:
            logger.info('Creating new configuration: %s' % filename)
            self.filename = filename

            # new configuration, add a minimal set of components to make it valid
            self.controller = Controller(self, 'Controller')
            self.manager = Manager('Manager', '127.0.0.1', '1940', 'bin/bziManager')
            self.manager.parent = self.modules

            # this parameter is required for the manager to function
            self.manager.add_softcycle()

    def basepath(self):
        """
        Directory containing the configuration file (usually the .xml files
        for the units will be in a subdirectory of this location)
        """
        return os.path.dirname(os.path.abspath(self.filename))

    def name(self):
        return os.path.splitext(os.path.split(self.filename)[1])[0]

    @staticmethod
    def create(filename):
        # create a minimal new configuration and save it to a file
        config = Configuration(filename)
        config.write()
        return config

    def __repr__(self):
        s    = 'Configuration: %s\n' % self.name()
        s   += ' basepath: %s\n' % self.basepath()
        s   += ' # modules: %d\n' % len(self.modules)
        s   += ' Module list:\n'
        for m in itervalues(self.modules):
            s += str(m)
        s += str(self.controller)
        s += str(self.manager)
        return s

    def next_free_port(self):
        """
        Returns next available module port number
        """
        if len(self.modules) == 0:
            return 1950
        return 1 + max([m.port for m in itervalues(self.modules)])

    def next_free_stage(self):
        """
        Returns next available stage number for sync modules (these should 
        apparently be consecutive so just return length of the list of sync
        modules here)
        """
        return len(self.sync_modules())

    def used_addresses(self):
        """
        Return list of ip:port strings for all modules and the manager instance,
        since it is just a special module
        """
        addresses = ['%s:%d' % (m.ip, m.port) for m in itervalues(self.modules)]
        addresses.append('%s:%d' % (self.manager.ip, self.manager.port))
        return addresses

    def used_stages(self):
        """
        Returns the list of stage values for all the sync modules in the configuration
        """
        return [x.stage for x in self.sync_modules()]

    def sync_modules(self):
        """ 
        Returns a list of all the sync modules in the configuration
        """
        return [x[1] for x in iteritems(self.modules) if isinstance(x[1], SyncModule)]

    def async_modules(self):
        """
        Returns a list of all the async modules in the configuration
        """
        return [x[1] for x in iteritems(self.modules) if isinstance(x[1], AsyncModule)]

    def sorted_sync_modules(self):
        """
        Returns a list of all the sync modules in order of their stage value
        """
        return sorted(self.sync_modules(), key=lambda x: x.stage)

    def units(self):
        """
        Returns a list of all the units in the configuration (these come from the
        main list of modules plus any units in the manager)
        """
        # manager can have units and isn't in self.modules
        return [u for u in itervalues(self.manager.units)] + [u for m in itervalues(self.modules) for u in itervalues(m.units)]

    def streams(self):
        """
        Returns a list of all the streams in the configuration (from the Controller
        and all defined units
        """
        all_streams = [s for s in itervalues(self.controller.inputs)] + [s for s in itervalues(self.controller.outputs)]
        units = self.units()
        all_streams += [s for u in units for s in u.inputs]
        all_streams += [s for u in units for s in u.outputs]
        return all_streams

    def input_streams(self):
        """
        Returns a dict of all input streams in the configuration
        """
        input_streams = {}
        for stream in [s for s in itervalues(self.controller.inputs)] + [s for u in self.units() for s in itervalues(u.inputs)]:
            input_streams[stream.name] = stream
        return input_streams

    def output_streams(self):
        """
        Returns a dict of all input streams in the configuration
        """
        output_streams = {}
        for stream in [s for s in itervalues(self.controller.outputs)] + [s for u in self.units() for s in itervalues(u.outputs)]:
            output_streams[stream.name] = stream
        return output_streams

    def all_parameters(self):
        """
        Returns a list of all parameters in the configuration
        """
        return [p for p in itervalues(self.manager.parameters)] + [p for u in self.units() for p in itervalues(u.parameters)]

    def unit_locations(self):
        """
        Returns a set of all distinct unit locations (directories containing .xml files)
        from the configuration.
        """
        return set([os.path.dirname(u.xmlpath) for u in self.units()])
        

    def parse(self, root):
        """
        Parse a full configuration from the XML document structure in <root>
        """
        logger.info('Parsing configuration from XML: %s' % (self.filename))
        config = root.find('configuration')
        logger.info('Configuration name is %s' % self.name())
        self.controller = Controller.parse(self, config.find('controller'))
        self.manager = Manager.parse(self, config.find('manager'))

        logger.info('Parsing module list for configuration')
        modules = config.findall('module')
        for m in modules:
            module = ModuleFactory.parse(self, m)
            self.modules[module.name] = module


    def validate(self):
        """
        Attempts to check for and list all potential problems in the current
        configuration and returns a list of tuples, containing error/warning messages
        and the object associated with that message (if any, might be None)
        """
        errors = []
        warnings = []

        def verify_nonempty(obj, text, message):
            if len(text) == 0:
                errors.append((message, obj))

        def verify_stream(stream):
            verify_nonempty(stream, stream.name, 'Stream name is empty')
            if isinstance(stream, EventStream):
                verify_nonempty(stream, stream.initvalue, 'Missing initValue for EventStream')
                # TODO verify type

        # controller: just check name 
        verify_nonempty(self.controller, self.controller.name, 'Controller name is empty')
        for name, stream in iteritems(self.controller.inputs):
            verify_stream(stream)

        # manager: check ip/name and verify that the required module+parameter are present
        verify_nonempty(self.manager, self.manager.name, 'Manager name is empty')
        verify_nonempty(self.manager, self.manager.ip, 'Manager IP/hostname is empty')
        if 'softCycle' not in self.manager.parameters:
            errors.append(('softCycle parameter missing from Manager!', self.manager))
        for name, param in iteritems(self.manager.parameters):
            verify_nonempty(param, param.name, 'Parameter name is empty')

        # modules: same as manager
        for name, mod in iteritems(self.modules):
            verify_nonempty(mod, name, 'Module name is empty')
            verify_nonempty(mod, mod.ip, 'Module IP is empty')

        # units: just name and class
        for unit in self.units():
            verify_nonempty(unit, unit.name, 'Unit name is empty')
            verify_nonempty(unit, unit.class_, 'Unit class is empty')
            for name, param in iteritems(unit.parameters):
                verify_nonempty(param, param.name, 'Parameter name is empty')

            # streams: name non-empty, init value, ...
            for name, stream in iteritems(unit.inputs):
                verify_stream(stream)

            for name, stream in iteritems(unit.outputs):
                verify_stream(stream)

        # do some basic checks on streams
        all_input_streams = self.input_streams()
        all_output_streams = self.output_streams()

        # every input stream in a unit should probably have a corresponding output stream 
        # in some other unit, or in the controller... make these warnings instead of
        # errors though just in case...
        for name, inp in iteritems(all_input_streams):
            if name not in all_output_streams:
                # special case for FileWriter, i think...
                if name != 'closeFile':
                    warnings.append(('Input stream %s in %s doesn\'t have a corresponding output stream' % (name, inp.parent.parent.name), stream.parent.parent))

        # do some basic parameter checking
        for p in self.all_parameters():
            if not p.validate():
                errors.append(('Parameter %s has an invalid value (type is %s)' % (p.name, BZI_TYPES[p.type_]), p))

        # check no address clashes occur
        # list of ip:port pairs for all modules+manager
        def get_dup_addrs(addrs):
            seen, seen2 = set(), set()
            for a in addrs:
                if a in seen:
                    seen2.add(a)
                else:
                    seen.add(a)
            return list(seen2)

        dup_addrs = get_dup_addrs(self.used_addresses())
        if len(dup_addrs) > 0:
            for dup in dup_addrs:
                errors.append(('Two or more modules detected using address %s' % dup, None))
        
        # check no stage clashes occur
        def get_dup_stages(stages):
            seen, seen2 = set(), set()
            for s in stages:
                if s in seen:
                    seen2.add(s)
                else:
                    seen.add(s)
            return list(seen2)
        all_stages = self.used_stages()
        if len(all_stages) > 0:
            dup_stages = get_dup_stages(self.used_stages())
            if len(dup_stages) > 0:
                for dup in dup_stages:
                    errors.append(('Two or more modules detected using stage %s' % dup, None))
            if min(all_stages) != 0:
                errors.append(('First module stage number is %d, it should be 0' % min(all_stages), None))

            if len(all_stages) != 1 + (max(all_stages) - min(all_stages)):
                all_stages.sort()
                errors.append(('Stage numbering should be consecutive, currently %s' % (str(all_stages)), None))

        logger.info('validated, %d errors, %d warnings' % (len(errors), len(warnings)))
        return (errors, warnings)


    def write(self, override_unitpath=None):
        """
        Generates the set of XML files representing the configuration, with the 
        location being based on self.filename
        """

        # write out unit XML files first. this is a little more complicated than
        # it should be because there's no real rule about where they should go, so:
        # - if there's a unitpath parameter supplied, write all units there
        # - otherwise:
        #   - if a unit has a non-empty xmlpath, attempt to use that
        #   - if no xmlpath, create one based on the location of the config file
        override = False
        unitpath = os.path.join(self.basepath(), self.name())
        if override_unitpath is not None:
            unitpath = override_unitpath
            logger.info('Override unit paths to %s' % (unitpath))
            override = True

        if not os.path.exists(unitpath):
            logger.info('Creating new directory')
            os.makedirs(unitpath)

        logger.info('Writing unit XML files...')

        for u in self.units():
            if u.xmlpath is None or len(u.xmlpath) == 0:
                # if the path isn't set, always use the main path or the 
                # override path if set
                logger.info('No XML path found for unit %s, generating new one' % (u.name))
                u.xmlpath = os.path.join(unitpath, u.name + '.xml')
            else:
                # if the path is set, use it except when an override path has 
                # been supplied instead
                if override:
                    logger.info('Overriding XML path for %s' % (u.name))
                    u.xmlpath = os.path.join(unitpath, u.name + '.xml')

            # writes out the file using the .xmlpath attribute
            u.write()

        # write top level XML
        logger.info('Writing configuration to XML %s' % self.filename)
        root = ET.Element('bziconfigurations')
        config = ET.Element('configuration', name=self.name())
        root.append(config)
        config.append(self.controller.xml(config))
        config.append(self.manager.xml(config))
        for m in self.sorted_sync_modules():
            config.append(m.xml(config))
        for m in self.async_modules():
            config.append(m.xml(config))
        with open(self.filename, 'wb') as f:
            tree = ET.ElementTree(root)
            tree.write(f, pretty_print=True, xml_declaration=True, encoding="UTF-8")


if __name__ == "__main__":
    con = Configuration(sys.argv[1])

    print('Configuration')
    print('\tController')
    for s in con.controller.inputs.keys():
        print('\t\t%s' % s)
    for s in con.controller.outputs.keys():
        print('\t\t%s' % s)
    print('\tManager')
    for u in con.manager.units.keys():
        print('\t\tU: %s' % u)
    for p in con.manager.parameters.keys():
        print('\t\tP: %s' % p)
    print('\tModules')
    for m in con.modules.keys():
        print('\t\t%s' % m)
        mod = con.modules[m]
        for u in mod.units.keys():
            print('\t\t\t%s' % u)
            unit = mod.units[u]
            for s in unit.inputs.keys():
                print('\t\t\t\t%s' % s)
            for s in unit.outputs.keys():
                print('\t\t\t\t%s' % s)

    print(con.used_addresses())
    print(con.unit_locations())

    con.write('c:\\temp')
    # con = Configuration.create('test.conf')
    # con.write()
